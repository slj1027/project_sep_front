const Koa = require('koa');
const app = new Koa();


const Route = require('koa-router')
const router = new Route();
const bodyparser = require('koa-bodyparser');
//引入sdk

const AlipaySDK = require('alipay-sdk').default;
const AlipayFormData = require('alipay-sdk/lib/form').default;

// 创建AlipaySDK
const alipaySdk = new AlipaySDK({
    appId: '2021000118642308', // 开放平台上创建应用时生成的 appId
    signType: 'RSA2', // 签名算法,默认 RSA2
    gateway: 'https://openapi.alipaydev.com/gateway.do', // 支付宝网关地址 ，沙箱环境下使用时需要修改
    alipayPublicKey:
        'MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAzllikj5/nQFzf4N2/NImQ2ubP6z1G0DjczDdstvKW94sLJGesb/g6yHOL8kXkF5WEiOoxB1rIT9Z8jsJ33YyNFbEFMbc0OlUXvEMRe9tI2UE555OgF0HV/vhK0kZ3o2RSQuWnFwJYLuqWWXOUCLrRanA/w9JJvm/si0gNSkEecznlj1I5ZVrVYQGahNC48TPjHOLMIDGz3BvaXw9occLF1fChvutYF+LqPaBGWio59R+mPQ6TCLj2aAWm3n2AJs8Sbdun4NPeY715rvgmpbyDE+5C/RENFi5Xw54sLOWnZ+nI1nSJf4M5U/Z+tunJgIdOkQhaVa8p+U28WtUAMbBgwIDAQAB', // 支付宝公钥，需要对结果验签时候必填
    privateKey:
        'MIIEpAIBAAKCAQEAq8BzKrtUFB1sdKECIm6eIi1wi3nTXX4pmD4SCU7LyuSWdU6uJRTBKy18PhkrXo9vpw3dFhQaQEjhm2Xj4siYnETJ6SGiXwXyz9WvCoP/kEIg/nfQTPSkYjFgvf4ajLrODhpUhmpuXu75niSkAxqvQfPjTuc0NC3mPdlGhYg0Iem/mZRLLjpB+DV5LOvYbWYWaz2ZiojAQe3FV+tbOrd1+SHz/H2ZxWHs0a8krEXAdyWqzIgEnSny9+dggJSQtENe5Ula9C3hvZZVsPUKY+jOSmOLslejjmnL42x87p1Jum+U6ueirZR3fvVxJJauBAZEka2eWoEpRXRrwhdf1/4H6QIDAQABAoIBAEMlAKEjDRK0EhNiYqg9YJrx8r7ESMqTKQW4tmpnIKrYRK3lrZUXkRElPh40zsyv/LFtbRJVl5GsLG8JHVcqnubwyZLXsFe/xpZT7pOI2R93itiQ1KFRPvaYYT7FmXGNEkNXLdZCw5VfjbyyjGAeTYfWJfZR0wnXK9zgFX7cqazbwv15aeGIcqz2PqW8E8FwI8li8goD8TUEeW8HkZG4xp6Nin54st1PXhtgYeRUFi9D54Fx/xh3u73slO/n+r8MCizCXHzbhNTHKWZ0hRYDRf/FZ6hlnTzOwd+ugpWrnJuLWX6KF5hzwmtPzMoxiPoFJrXJw+LlxmCea6rvcFAjntECgYEA/4yd4zoG18uM/qdD+dGGtE0HM6rldfm6xcV+A0AMK3OnDQ3IQkV3pu3e7PQ8kmJq5Biay4hGzkEuRxyy+Y4p3zcqHNX9ESWgBetOg9FthPY3Kf7oX+lt9UVIFidcGr9ttAy3SpcQEpjkzMj2rK5OuVqErG9/EBIi6yQThxiAza0CgYEArA3/ZSgf/RRwVB0iXTrX3gBKKt9UCv6NhQHqZ7QQRlyNU5VMvoZq35ON5j0WQQ/XvBaKbblIvOwdsrMTu5azCmNtjlHof0wdTD+w8RRZ+UEI92Zs0eTAYVwK5lT3ZrkkTL2UaSDntDaBv4uxPUe1Cd9G+82iu+IRKY4bZRZMcq0CgYB/ByYAqnp2UE9Q0lJe3wEnpNZ8n2maYD2wS4BdGzwxaTbTrVMrZh6ZEsrqXGmzlkpuAbEIlUAlDI91d9WbaUh1FZXQaap3GI3YA2Su766SFlXLgVN/QaiP8BVqNbx560gb5OpDoJ2zsN6i0Q6VPbwSNOWYVYACvBExc2hlkg+MrQKBgQCdlNRtWaqyqJ2Y7HYk3VgIxEspqRHYX/xN7cs63/hn+SyXeQ6pEYOg8gvs69YOEOyOIDb499Je9gLdQJa6IPQqd5ZH200OCjBrPYpZw0IEwEPuE6lnNBPfF+YDv544Pym3/4qGHtUYgTOks9TJQ3vUZuMkjNqXKh596OG+XAX4oQKBgQCMV6kFGzvesfS1zAHlkNlvioU5JzUYpYYofYpGm11C5lVLyriqVCmvMhSFQzpSnrDD7OsToP1kHsmUS4ALG/amT7jBXhOrzdyo0BGS24mnbs66Py0E+JCeTKnVBPXDMy3P4sgtyBWnz8Nc6w16WnMxu9m3NQZONhHRpUMwixhhhg==', // 应用私钥字符串
});

let num = '';
for (var i = 0; i < 14; i++) {
    if (i == 0) {
        num += Math.floor(Math.random() * 9 + 1);
    } else { num += Math.floor(Math.random() * 10); }
}

//支付接口
router.post('/pay/pay', async ctx => {
    const {url, totalAmount} = ctx.request.body
    const formData = new AlipayFormData();
    formData.setMethod('get');
    formData.addField('notifyUrl', 'https://www.baidu.com');
    formData.addField('bizContent', {
        outTradeNo: num, // 商户订单号,64个字符以内、可包含字母、数字、下划线,且不能重复
        productCode: 'FAST_INSTANT_TRADE_PAY', // 销售产品码，与支付宝签约的产品码名称,仅支持FAST_INSTANT_TRADE_PAY
        totalAmount: (totalAmount*1).toFixed(2), // 订单总金额，单位为元，精确到小数点后两位
        subject: '商品', // 订单标题
        body: '商品详情', // 订单描述
    });
    formData.addField('returnUrl', url);
    const result = await alipaySdk.exec(
        // result 为可以跳转到支付链接的 url
        'alipay.trade.page.pay', // 统一收单下单并支付页面接口
        {}, // api 请求的参数（包含“公共请求参数”和“业务参数”）
        { formData: formData }
    );
    ctx.body = {
        code: 1,
        result,
    };
});

app.use(bodyparser()).use(router.routes());
app.listen(9000, () => {
    console.log(`service is running at http://localhost:9000/pay`)
});