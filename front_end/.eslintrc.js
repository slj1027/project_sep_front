module.exports = {
  extends: ['react-app', 'react-app/jest', 'prettier'],
  plugins: ['prettier'],
  rules: {
    'no-console': process.env.NODE_ENV === 'development' ? 'off' : 'off',
    'no-debugger': process.env.NODE_ENV === 'development' ? 'off' : 'off',
    'no-unused-vars': 'error',
    'prettier/prettier': [
      'error',
      {
        singleQuote: true,
        printWidth: 100,
        tabWidth: 2,
        trailingComma: 'es5',
        endOfLine: 'auto',
        semi: true,
        jsxSingleQuote: true,
      },
    ],
  },
};
