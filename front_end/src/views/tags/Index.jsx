import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { Col, Row, Affix } from 'antd';
import { getTagItem, getTagArticleItem, getTag, setView } from '../../actions';
import RecommendToReading from '../../components/recommendToReading/Index';
import CategoryTitle from '../../components/categoryTitle/Index';
import Share from '../../components/share/Index';
import List from '../../components/list/Index';
import Loading from '../../components/loading/Index';
// import style from '../../styles/style.module.min.css';
import './index.min.css';
function Tags() {
  const param = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    dispatch(getTag());
  }, [dispatch]);
  useEffect(() => {
    dispatch(getTagItem(param.id));
    dispatch(getTagArticleItem(param.id, 1, 12, 'publish'));
  }, [dispatch, param.id]);
  const { statusCode, data: tag } = useSelector((state) => state.article.tag);
  const { data: tagItem } = useSelector((state) => state.article.tagItem);
  const { statusCode: statusCode1, data: tagArticleItem } = useSelector(
    (state) => state.article.tagArticleItem
  );
  const [dis, setDis] = useState(false);
  const [showItem, setShowItem] = useState({});
  const handleCancel = () => {
    setDis(false);
  };
  const handleOk = () => {
    setDis(true);
  };
  let toDetail = (item) => {
    navigate('/detail/' + item.id);
    dispatch(setView('https://creation.shbwyz.com/article/' + item.id));
  };
  return (
    <div className='tags'>
      <Row justify='space-around'>
        <Col
          xs={{ span: 24, offset: 0 }}
          sm={{ span: 24, offset: 0 }}
          md={{ span: 15, offset: 0 }}
          lg={{ span: 15, offset: 0 }}
          xl={{ span: 17, offset: 0 }}
          className='tag_left'
        >
          <Loading className='tag_content' code={statusCode}>
            <div className='tag_top'>
              {tagItem && (
                <p>
                  yu <b>{tagItem.label}</b> tagRelativeArticles
                </p>
              )}
              {tagArticleItem && (
                <p>
                  total Search <b>{tagArticleItem[1]}</b> piece
                </p>
              )}
            </div>
            <div className='tag_middle'>
              <h3>tagTitle</h3>
              <div>
                {tag &&
                  tag.length > 0 &&
                  tag.map((tag, i) => {
                    return (
                      <div key={i}>
                        <a
                          href={'/tags/' + tag.value}
                          className={tagItem && tag.label === tagItem.label ? 'show' : ''}
                        >
                          <span>{tag.label}</span>
                          <span>[{tag.articleCount}]</span>
                        </a>
                      </div>
                    );
                  })}
              </div>
            </div>
            <Loading className='tag_bottom' code={statusCode1}>
              {tagArticleItem && (
                <List
                  arr={tagArticleItem[0]}
                  setShow={(item) => setShowItem(item)}
                  setDisplay={handleOk}
                  toDetail={toDetail}
                />
              )}
            </Loading>
          </Loading>
        </Col>
        <Col
          xs={{ span: 0, offset: 0 }}
          sm={{ span: 0, offset: 0 }}
          md={{ span: 8, offset: 1 }}
          lg={{ span: 8, offset: 1 }}
          xl={{ span: 6, offset: 1 }}
        >
          <Affix offsetTop={0}>
            <div>
              <RecommendToReading></RecommendToReading>
              <CategoryTitle></CategoryTitle>
            </div>
          </Affix>
        </Col>
        <Share item={showItem} handleCancel={handleCancel} handleOk={handleOk} isModalOpen={dis} />
      </Row>
    </div>
  );
}

export default Tags;
