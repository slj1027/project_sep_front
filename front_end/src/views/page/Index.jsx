import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams, useNavigate } from 'react-router';
import { getRecommendToReading, GET_PAGE_ITEM, setView } from '../../actions';
import Comment from '../../components/comment/Index';
import CommentContent from '../../components/commentContent/Index';
import Share from '../../components/share/Index';
import List from '../../components/list/Index';
import Loading from '../../components/loading/Index';

import './scss/index.min.css';
function Page() {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    dispatch(getRecommendToReading('publish'));
  }, [dispatch]);
  const { id } = useParams();
  useEffect(() => {
    dispatch(GET_PAGE_ITEM(id));
  }, [dispatch, id]);
  const { statusCode: statusCode1, data: pageItem } = useSelector(
    (state) => state.article.pageItem
  );
  const { statusCode, data: recommendToReading } = useSelector(
    (state) => state.article.recommendToReading
  );
  const [dis, setDis] = useState(false);
  const [showItem, setShowItem] = useState({});
  const handleCancel = () => {
    setDis(false);
  };
  const handleOk = () => {
    setDis(true);
  };
  let toDetail = (item) => {
    navigate('/detail/' + item.id);
    dispatch(setView('https://creation.shbwyz.com/article/' + item.id));
  };
  return (
    <Loading code={statusCode1}>
      {pageItem && (
        <div className='page'>
          {pageItem.cover && (
            <div
              style={{
                backgroundImage: 'url(' + pageItem.cover + ')',
                backgroundSize: 'cover',
                backgroundPosition: '50%',
                width: '100%',
                height: '350px',
              }}
            ></div>
          )}

          <div dangerouslySetInnerHTML={{ __html: pageItem.html }} className='markdown'></div>
        </div>
      )}
      <div className='comment'>
        <h3>评论</h3>
        <Comment type={'page'}></Comment>
        <CommentContent type={'page'}></CommentContent>
      </div>
      <div className='read'>
        <h3>推荐阅读</h3>
        <List
          arr={recommendToReading}
          setShow={(item) => setShowItem(item)}
          setDisplay={() => setDis(true)}
          toDetail={toDetail}
          statusCode={statusCode}
        />
        <Share item={showItem} handleCancel={handleCancel} handleOk={handleOk} isModalOpen={dis} />
      </div>
    </Loading>
  );
}

export default Page;
