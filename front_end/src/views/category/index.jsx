import React, { useEffect, useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { useParams, useNavigate } from 'react-router-dom';
import { Col, Row, Affix } from 'antd';
import { getCategoryTitle, getCategoryItem, getArtCate, setView } from '../../actions';
import RecommendToReading from '../../components/recommendToReading/Index';
import TagTitle from '../../components/tagTitle/Index';
import List from '../../components/list/Index';
import Share from '../../components/share/Index';
import Loading from '../../components/loading/Index';
import './index.min.css';
function Category() {
  const param = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  useEffect(() => {
    dispatch(getCategoryItem(param.id));
    dispatch(getArtCate(param.id));
    dispatch(getCategoryTitle());
  }, [dispatch, param.id, param.value]);
  const { data: category } = useSelector((state) => state.article.categoryTitle);
  const { statusCode, data: cateItem } = useSelector((state) => state.article.categoryItem);
  const { statusCode: statusCode1, data: artCate } = useSelector(
    (state) => state.article.artCateList
  );
  console.log(artCate);
  const [dis, setDis] = useState(false);
  const [showItem, setShowItem] = useState({});
  const handleCancel = () => {
    setDis(false);
  };
  const handleOk = () => {
    setDis(true);
  };
  let toDetail = (item) => {
    navigate('/detail/' + item.id);
    dispatch(setView('https://creation.shbwyz.com/article/' + item.id));
  };
  return (
    <div className='category'>
      <Row justify='space-around'>
        <Col
          xs={{ span: 24, offset: 0 }}
          sm={{ span: 24, offset: 0 }}
          md={{ span: 15, offset: 0 }}
          lg={{ span: 15, offset: 0 }}
          xl={{ span: 17, offset: 0 }}
          className='category_left'
        >
          <Loading className='category_content' code={statusCode}>
            {cateItem && (
              <div className='category_top'>
                <p>
                  yu <b>{cateItem.label}</b> tagRelativeArticles
                </p>
                <p>
                  total Search <b>{artCate && artCate[1]}</b> piece
                </p>
              </div>
            )}
            <div className='category_title'>
              <span>
                <a href={'/'}>
                  <span>all</span>
                </a>
              </span>
              {category && category.length > 0 ? (
                category.map((category, i) => {
                  return (
                    <span key={i}>
                      <a
                        href={'/category/' + category.value}
                        className={category.label === cateItem.label ? 'show' : ''}
                      >
                        <span>{category.label}</span>
                      </a>
                    </span>
                  );
                })
              ) : (
                <div>empty</div>
              )}
            </div>
            {artCate && (
              <div className='category_bottom'>
                <List
                  arr={artCate[0]}
                  setShow={(item) => setShowItem(item)}
                  setDisplay={handleOk}
                  toDetail={toDetail}
                  statusCode={statusCode1}
                />
              </div>
            )}
          </Loading>
        </Col>
        <Col
          xs={{ span: 0, offset: 0 }}
          sm={{ span: 0, offset: 0 }}
          md={{ span: 8, offset: 1 }}
          lg={{ span: 8, offset: 1 }}
          xl={{ span: 6, offset: 1 }}
        >
          <Affix offsetTop={0}>
            <div>
              <RecommendToReading></RecommendToReading>
              <TagTitle></TagTitle>
            </div>
          </Affix>
        </Col>
        <Share item={showItem} handleCancel={handleCancel} handleOk={handleOk} isModalOpen={dis} />
      </Row>
    </div>
  );
}

export default Category;
