/* knowledge 页面 */
import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { Row, Col, Affix } from 'antd';
import RecommendToReading from '../../components/recommendToReading/Index';
import CategoryTitle from '../../components/categoryTitle/Index';
import Share from '../../components/share/Index';
import List from '../../components/list/Index';
import * as api from '../../actions/index';
import './css/index.css';
function Knowledge() {
  //数据派发
  const dispatch = useDispatch();
  const navigate = useNavigate();
  //跳转详情
  let toDetail = (item) => {
    navigate('/particulars/' + item.id);
    dispatch(api.setView('https://creation.shbwyz.com/knowledge/' + item.id));
  };
  const { statusCode, data: list } = useSelector((state) => state.article.know);
  //默认数据渲染
  useEffect(() => {
    dispatch(api.get_list());
  }, [dispatch]);
  const [dis, setDis] = useState(false);
  const [showItem, setShowItem] = useState({});
  const handleCancel = () => {
    setDis(false);
  };
  const handleOk = () => {
    setDis(true);
  };
  return (
    <Row justify='space-around'>
      <Col
        xs={{ span: 24, offset: 0 }}
        sm={{ span: 24, offset: 0 }}
        md={{ span: 15, offset: 0 }}
        lg={{ span: 15, offset: 0 }}
        xl={{ span: 17, offset: 0 }}
        className='box'
      >
        {
          <List
            arr={list && list[0]}
            setShow={(item) => setShowItem(item)}
            setDisplay={() => setDis(true)}
            toDetail={toDetail}
            statusCode={statusCode}
          />
        }
      </Col>

      <Col
        xs={{ span: 0, offset: 0 }}
        sm={{ span: 0, offset: 0 }}
        md={{ span: 8, offset: 1 }}
        lg={{ span: 8, offset: 1 }}
        xl={{ span: 6, offset: 1 }}
      >
        <Affix offsetTop={0}>
          <div>
            <RecommendToReading />
            <CategoryTitle />
          </div>
        </Affix>
      </Col>
      <Share item={showItem} handleCancel={handleCancel} handleOk={handleOk} isModalOpen={dis} />
    </Row>
  );
}

export default Knowledge;
