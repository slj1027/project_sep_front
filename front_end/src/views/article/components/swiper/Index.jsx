//*轮播图
import React from 'react';
import { useNavigate } from 'react-router';
import { moment } from '../../../../js/timer';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay, Pagination } from 'swiper';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css';
function Carousel({ arr }) {
  const navigate = useNavigate();
  return (
    <Swiper
      slidesPerView={1}
      spaceBetween={30}
      loop={arr.length > 1}
      pagination={{
        clickable: true,
      }}
      navigation={true}
      modules={[Pagination, Autoplay]}
      autoplay={
        arr.length > 1 && {
          delay: 2500,
          disableOnInteraction: false,
        }
      }
      className='mySwiper'
    >
      {arr &&
        arr.map((item) => {
          return (
            item.cover && (
              <SwiperSlide
                style={{
                  backgroundImage: 'url(' + item.cover + ')',
                  backgroundSize: 'cover',
                  backgroundPosition: '50%',
                  color: '#999',
                  fontSize: '30px',
                }}
                key={item.id}
                onClick={() => {
                  navigate('/detail/' + item.id);
                }}
              >
                <div>
                  <h2 style={{ fontSize: '20px' }}>{item.title}</h2>
                  <p style={{ fontSize: '14px' }}>
                    {moment(item.publishAt).fromNow()}|{item.views}次阅读
                  </p>
                </div>
              </SwiperSlide>
            )
          );
        })}
    </Swiper>
  );
}

export default Carousel;
