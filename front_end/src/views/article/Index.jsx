/* Article页面  */
import React, { useEffect, useState } from 'react';
import { NavLink, useNavigate } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { Col, Row, Affix } from 'antd'; //引入antd组件
import * as api from '../../actions/index'; //派发仓库方法
//引入组件
import RecommendToReading from '../../components/recommendToReading/Index';
import Share from '../../components/share/Index';
import TagTitle from '../../components/tagTitle/Index';
import Swiper from './components/swiper/Index';
import List from '../../components/list/Index';
import Loading from '../../components/loading/Index';

import InfiniteScroll from 'react-infinite-scroll-component'; //滚动加载
import './index.min.css'; //引入样式
function Article() {
  const disPath = useDispatch();
  const navigate = useNavigate();
  const { statusCode, data: list } = useSelector((state) => state.article.data);
  const { data: listButton } = useSelector((state) => state.article.SwiperDataButton);
  //*滚动加载
  const [hasMore, setHasMore] = useState(true);
  const [page, setPage] = useState(3);
  useEffect(() => {
    disPath(api.getData({ status: 'publish' }));
    disPath(api.Get_SwiperButton());
  }, [disPath]);
  const fetchMoreData = () => {
    let time = null;
    if (page >= list[1]) {
      setHasMore(false);
      clearInterval(time);
    }
    time = setInterval(() => {
      setPage(page + 2);
    }, 300);
  };
  const [dis, setDis] = useState(false);
  const [showItem, setShowItem] = useState({});
  const handleCancel = () => {
    setDis(false);
  };
  const handleOk = () => {
    setDis(true);
  };
  let toDetail = (item) => {
    navigate('/detail/' + item.id);
    disPath(api.setView('https://creation.shbwyz.com/article/' + item.id));
  };
  return (
    <Row justify='space-around'>
      <Col
        xs={{ span: 24, offset: 0 }}
        sm={{ span: 24, offset: 0 }}
        md={{ span: 15, offset: 0 }}
        lg={{ span: 15, offset: 0 }}
        xl={{ span: 17, offset: 0 }}
      >
        <Loading className='article' code={statusCode}>
          {/* 轮播图 */}

          <div className='article_top'>
            {list &&
              list[0] &&
              list[0].filter((item) => item.isRecommended && item.cover).length > 0 && (
                <Swiper arr={list[0].filter((item) => item.isRecommended && item.cover)} />
              )}
          </div>

          <div className='article_tab'>
            {/* tab切换 */}
            <div className='article_tab_button'>
              <span
                onClick={() => {
                  navigate('/');
                }}
                style={{ color: '#ff0055' }}
              >
                所有
              </span>
              {listButton &&
                listButton.map((item, index) => {
                  return (
                    <span key={index}>
                      <NavLink className='ac' to={`/category/${item.value}`}>
                        {item.label}
                      </NavLink>
                    </span>
                  );
                })}
            </div>
            {/* 文章渲染 */}
            <div className='article_content'>
              {list && list[0].length > 0 && (
                <InfiniteScroll /* 滚动加载 */
                  dataLength={page}
                  hasMore={hasMore}
                  next={fetchMoreData}
                  loader={<h4 style={{ textAlign: 'center' }}>数据正在加载中</h4>}
                  endMessage={
                    <p style={{ textAlign: 'center' }}>
                      <b>数据已经全部加载完毕，更多精彩敬请期待...</b>
                    </p>
                  }
                >
                  {
                    <List
                      arr={list[0].slice(0, page)}
                      setShow={(item) => setShowItem(item)}
                      setDisplay={() => setDis(true)}
                      toDetail={toDetail}
                      statusCode={statusCode}
                    />
                  }
                </InfiniteScroll>
              )}
            </div>
          </div>
        </Loading>
      </Col>
      <Col
        xs={{ span: 0, offset: 0 }}
        sm={{ span: 0, offset: 0 }}
        md={{ span: 8, offset: 1 }}
        lg={{ span: 8, offset: 1 }}
        xl={{ span: 6, offset: 0 }}
      >
        <Affix offsetTop={0}>
          <div>
            <RecommendToReading />
            <TagTitle />
          </div>
        </Affix>
      </Col>
      <Share item={showItem} handleCancel={handleCancel} handleOk={handleOk} isModalOpen={dis} />
    </Row>
  );
}

export default Article;
