/* Archives页面 */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
//公用组件
import RecommendToReading from '../../components/recommendToReading/Index';
import Category from '../../components/categoryTitle/Index';
import Loading from '../../components/loading/Index';
import { Col, Row, Affix } from 'antd';
import { moment } from '../../js/timer';
import * as api from '../../actions/index';
import './css/archives.css';
function Archives() {
  //数据派发
  const dispatch = useDispatch();
  //跳路由
  const navigate = useNavigate();
  //默认数据渲染
  useEffect(() => {
    //数据派发
    dispatch(api.get_archives());
  }, [dispatch]);

  //从仓库得到数据
  const { statusCode, data: list } = useSelector((state) => state.article.archives);
  return (
    <Row>
      <Col
        xs={{ span: 24, offset: 0 }}
        sm={{ span: 24, offset: 0 }}
        md={{ span: 15, offset: 0 }}
        lg={{ span: 15, offset: 0 }}
        xl={{ span: 17, offset: 0 }}
      >
        <Loading code={statusCode} className='archives_content'>
          <div className='archivesTop'>
            <h1>archives</h1>
            <h3>
              total
              {list instanceof Object &&
                Object.keys(list).map((item, index) => {
                  return (
                    <span key={index}>
                      {Object.keys(list[item]).map((a, b) => {
                        return <b key={b}>{list[item][a].length}</b>;
                      })}
                    </span>
                  );
                })}
              piece
            </h3>
          </div>
          <div className='archives'>
            {list instanceof Object &&
              Object.keys(list).map((item, index) => {
                return (
                  <div key={index}>
                    <h2>{item}</h2>
                    {Object.keys(list[item]).map((a, b) => {
                      return (
                        <div key={b} className='arc'>
                          <h3>{a}</h3>
                          <ul>
                            {list[item][a] &&
                              list[item][a].map((v, index) => {
                                return (
                                  <li
                                    style={{
                                      animation: `example1 ${(index + 1) * 0.1}s ease-out ${
                                        (index + 2) * 0.1
                                      }s backwards`,
                                    }}
                                    key={index}
                                    onClick={() => {
                                      navigate('/detail/' + v.id);
                                      dispatch(
                                        api.setView('https://creation.shbwyz.com/article/' + v.id)
                                      );
                                    }}
                                  >
                                    <b>{moment(v.createAt).format('MM-DD')}</b>
                                    {/* 标题 */}
                                    <span>{v.title}</span>
                                  </li>
                                );
                              })}
                          </ul>
                        </div>
                      );
                    })}
                  </div>
                );
              })}
          </div>
        </Loading>
      </Col>
      <Col
        xs={{ span: 0, offset: 0 }}
        sm={{ span: 0, offset: 0 }}
        md={{ span: 8, offset: 1 }}
        lg={{ span: 8, offset: 1 }}
        xl={{ span: 6, offset: 0.5 }}
      >
        <Affix offsetTop={0}>
          <div>
            <RecommendToReading />
            <Category />
          </div>
        </Affix>
      </Col>
    </Row>
  );
}
export default Archives;
