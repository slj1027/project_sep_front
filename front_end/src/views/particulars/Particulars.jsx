/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { moment } from '../../js/timer';
import { EyeOutlined, ShareAltOutlined } from '@ant-design/icons';
import { Affix, Row, Col, Button } from 'antd';
import { useDispatch } from 'react-redux';
import { get_list, setView, get_knowledgeItem } from '../../actions';
import Share from '../../components/share/Index';
import Loading from '../../components/loading/Index';
import './css/index.min.css';
function Particulars() {
  // 获取路由传过来的参数
  const navigate = useNavigate();
  const param = useParams();
  const dispatch = useDispatch();
  // 从仓库得到数据
  const list = useSelector((state) => state.article.knowSlice);
  const show = useSelector((state) => state.article.knowledgeItem);
  const { statusCode } = useSelector((state) => state.article.know);
  const get = async () => {
    await dispatch(get_list());
    await dispatch(get_knowledgeItem(param.id));
  };
  useEffect(() => {
    get();
  }, []);
  const [dis, setDis] = useState(false);
  const [showItem, setShowItem] = useState({});
  const handleCancel = () => {
    setDis(false);
  };
  const handleOk = () => {
    setDis(true);
  };
  return (
    <Loading code={statusCode} className='particulars'>
      <Row justify='space-around'>
        {show && (
          <Col xs={{ span: 24, offset: 0 }} lg={{ span: 15, offset: 0 }}>
            <div className='par_title'>
              <h3>知识小册/{show.title}</h3>
              <h3>{show.title}</h3>
            </div>
            <div className='pars'>
              <div>
                <img src={show.cover} />
              </div>
              <div>
                <h3>{show.title}</h3>
                <p>
                  {show.views} readingCount {new Date(show.publishAt).toLocaleString()}
                </p>
                <div>
                  <Button type='primary' danger disabled>
                    开始阅读
                  </Button>
                </div>
                <p>pleaseWait</p>
                <div className='par_children'>
                  {show.children &&
                    show.children.length > 0 &&
                    show.children.map((item) => {
                      return (
                        <div key={item.id} className='par_children_item'>
                          <div>{item.title}</div>
                          <div className='control_item_show'>
                            {moment(item.publishAt).format('YYYY-MM-DD HH:mm:SS')}
                          </div>
                        </div>
                      );
                    })}
                </div>
              </div>
            </div>
          </Col>
        )}
        <Col xs={{ span: 24, offset: 0 }} lg={{ span: 8, offset: 1 }}>
          <Affix offsetTop={0}>
            <div>
              <h3 className='par_title'>其他知识笔记</h3>
              <div className='par_right'>
                {list &&
                  list.map((item, index) => {
                    return (
                      <div
                        key={index}
                        className='par_item'
                        onClick={() => {
                          navigate('/particulars/' + item.id);
                          dispatch(
                            setView(
                              'https://creation.shbwyz.com/knowledge/' + item.id,
                              'know',
                              item.id
                            )
                          );
                          dispatch(get_knowledgeItem(item.id));
                        }}
                      >
                        <h3>
                          <span>{item.title} </span> <b>| {moment(item.publishAt).fromNow()}</b>
                        </h3>
                        <dl>
                          <dt>
                            <img src={item.cover} alt='' />
                          </dt>
                          <dd>
                            <p>
                              <span> {item.summary}</span>
                            </p>
                            <p>
                              <b>
                                <EyeOutlined />
                                <span>{item.views}</span>
                              </b>
                              ·
                              <b
                                onClick={(e) => {
                                  e.stopPropagation();
                                  setDis(true);
                                  setShowItem(item);
                                }}
                              >
                                <ShareAltOutlined />
                                <span>分享</span>
                              </b>
                            </p>
                          </dd>
                        </dl>
                      </div>
                    );
                  })}
              </div>
            </div>
          </Affix>
        </Col>
        <Share item={showItem} handleCancel={handleCancel} handleOk={handleOk} isModalOpen={dis} />
      </Row>
    </Loading>
  );
}

export default Particulars;
