import React from 'react';
import { Button, Result } from 'antd';
import { useNavigate } from 'react-router';
function NoFound() {
  const nav = useNavigate();
  return (
    <Result
      className='result'
      status='404'
      title='404'
      subTitle='Sorry, the page you visited does not exist.'
      extra={
        <Button
          type='primary'
          onClick={() => {
            nav('/');
          }}
        >
          回首页
        </Button>
      }
    />
  );
}

export default NoFound;
