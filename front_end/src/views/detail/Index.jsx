/* 详情页面 */
import React, { useEffect, useState } from 'react';
import Comment from '../../components/comment/Index';
import CommentContent from '../../components/commentContent/Index';
import { Col, Row, Affix, Anchor, BackTop, message, Modal, Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { getDetailArticle } from '../../actions';
import { useNavigate, useParams } from 'react-router-dom';
import RecommendToReading from '../../components/recommendToReading/Index';
import Loading from '../../components/loading/Index';
import Action from './components/Action';
import './scss/index.min.css';
import { pay, psw } from '../../api';
const { Link } = Anchor;
function Detail() {
  const navigate = useNavigate();
  const param = useParams();
  const dispatch = useDispatch();
  const { statusCode, data: articleDetail } = useSelector((state) => state.article.articleDetail);
  useEffect(() => {
    dispatch(getDetailArticle(param.id));
  }, [dispatch, param.id]);
  const handleClick = (e, link) => {
    e.preventDefault();
    console.log(link);
  };
  //*支付弹窗

  const [isPayModalOpen, setIsPayModalOpen] = useState(false);
  const showPayModal = () => {
    setIsPayModalOpen(true);
  };
  useEffect(() => {
    if (articleDetail && articleDetail.totalAmount) {
      showPayModal();
    }
  }, [articleDetail]);

  const handlePayOk = async () => {
    setIsPayModalOpen(false);
    let { data } = await pay({
      id: param.id,
      totalAmount: articleDetail.totalAmount,
      url: `http://localhost:3000/detail/${param.id}`,
    });
    window.location.href = data.result;
  };

  const handlePayCancel = () => {
    setIsPayModalOpen(false);
    navigate('/');
  };
  //*支付弹窗
  const [isPswModalOpen, setIsPswModalOpen] = useState(false);
  const [password, setPsw] = useState(false);
  const showPswModal = () => {
    setIsPswModalOpen(true);
  };
  useEffect(() => {
    if (articleDetail && articleDetail.needPassword) {
      showPswModal();
    }
  }, [articleDetail]);

  const handlePswOk = async () => {
    let { data } = await psw(param.id, password);
    if (data.pass) {
      message.success('输入密码正确');
      setIsPswModalOpen(false);
    } else {
      message.error('输入密码错误，请重新输入');
    }
  };
  const handlePswCancel = () => {
    setIsPswModalOpen(false);
    navigate('/');
  };
  return (
    <div className='detail_content'>
      {articleDetail && articleDetail.totalAmount && (
        <Modal
          title='确认以下收费信息'
          open={isPayModalOpen}
          onOk={handlePayOk}
          onCancel={handlePayCancel}
          okText='去支付'
          cancelText='取消'
          type='primary'
        >
          <p style={{ textAlign: 'center' }}>支付金额:￥{articleDetail.totalAmount}</p>
        </Modal>
      )}
      {articleDetail && articleDetail.needPassword && (
        <Modal
          title='文章受保护，请输入访问密码'
          open={isPswModalOpen}
          onOk={handlePswOk}
          onCancel={handlePswCancel}
          okText='确定'
          cancelText='回首页'
          type='primary'
        >
          <div className='detail_psw'>
            <div>密码：</div>
            <div>
              <Input.Password onInput={(e) => setPsw(e.target.value)} />
            </div>
          </div>
        </Modal>
      )}
      <Row justify='space-around'>
        <Col
          xs={{ span: 24, offset: 0 }}
          sm={{ span: 24, offset: 0 }}
          md={{ span: 15, offset: 0 }}
          lg={{ span: 15, offset: 0 }}
          xl={{ span: 17, offset: 0 }}
          className='articleDetail'
        >
          {/* 操作（点赞，分享） */}
          <div className='div'>
            {articleDetail && <Action articleDetail={articleDetail} id={param.id} />}
          </div>

          {/* 详情页面数据渲染 */}
          <Loading code={statusCode}>
            {articleDetail && (
              <div className='detail'>
                <div className='detail_cover'>
                  <img src={articleDetail.cover} alt='' />
                </div>
                <h3>
                  <p>{articleDetail.title}</p>
                  <p>
                    <i>
                      publish at {articleDetail.publishAt} · readings
                      {articleDetail.views}
                    </i>
                  </p>
                </h3>
                {/* markdown内容渲染 */}
                <div
                  dangerouslySetInnerHTML={{
                    __html: articleDetail.html,
                  }}
                  className='markdown'
                ></div>
                {/* 复制按钮 */}
                {/* <div className='copy'>
                  <CopyToClipboard
                    text='<iframe src="//player.bilibili.com/player.html?aid=77737877&bvid=BV1xJ411z7eS&cid=132993821&page=1" scrolling="no" border="0" frameborder="no" framespacing="0" allowfullscreen="true"> </iframe>'
                    onCopy={() => {
                      message.success('复制成功');
                    }}
                  >
                    <button>copy</button>
                  </CopyToClipboard>
                </div> */}
              </div>
            )}
            {/* 评论区域 */}
            {
              <div className='detail_comment'>
                <h2>评论</h2>
                <Comment />
                <CommentContent />
              </div>
            }
          </Loading>
        </Col>
        <Col
          xs={{ span: 0, offset: 0 }}
          sm={{ span: 0, offset: 0 }}
          md={{ span: 8, offset: 1 }}
          lg={{ span: 8, offset: 1 }}
          xl={{ span: 6, offset: 1 }}
        >
          {/* 左侧吸顶 */}
          <Affix offsetTop={0}>
            <div>
              <RecommendToReading></RecommendToReading>
              <div>
                {/* 楼层 */}
                <h3>目录</h3>
                <Anchor onClick={handleClick}>
                  {articleDetail &&
                    articleDetail['toc'] &&
                    JSON.parse(articleDetail['toc']).map((item) => {
                      return (
                        <div style={{ marginLeft: 10 * (item.level - 1) + 'px' }} key={item.id}>
                          <Link href={'#' + item.id} title={item.text} />
                        </div>
                      );
                    })}
                </Anchor>
              </div>
            </div>
          </Affix>
        </Col>
        <BackTop /> {/* 吸顶 */}
      </Row>
    </div>
  );
}

export default Detail;
