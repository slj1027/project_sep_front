import React, { useState } from 'react';
import { WechatOutlined, ShareAltOutlined, HeartOutlined } from '@ant-design/icons';
import Share from '../../../components/share/Index';
import { getDetailArticleLike } from '../../../actions';
import { useDispatch } from 'react-redux';
function Action({ articleDetail, id }) {
  const dispatch = useDispatch();
  const [dis, setDis] = useState(false);
  const handleCancel = () => {
    setDis(false);
  };
  const handleOk = () => {
    setDis(false);
  };

  const zan = (item) => {
    dispatch({
      type: 'SET_Article_Likes',
      payload: item.id,
    });
    let flag = JSON.parse(localStorage.getItem('like')).some((a) => {
      return a === item.id;
    });
    dispatch(getDetailArticleLike(id, flag ? 'like' : 'dislike'));
  };
  return (
    <div className='action'>
      <div style={{ position: 'relative' }}>
        <div onClick={() => zan(articleDetail)}>
          <span
            className={
              JSON.parse(localStorage.getItem('like')).some((a) => {
                return a === articleDetail.id;
              })
                ? 'active'
                : ''
            }
          >
            <HeartOutlined />
          </span>
          <span className='num'>{articleDetail.likes}</span>
        </div>
      </div>
      <a href='#comment'>
        <WechatOutlined />
      </a>
      <ShareAltOutlined
        onClick={() => {
          setDis(true);
        }}
      />
      <Share
        item={articleDetail}
        handleCancel={handleCancel}
        handleOk={handleOk}
        isModalOpen={dis}
      />
    </div>
  );
}

export default Action;
