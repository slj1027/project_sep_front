import axios from 'axios';

const request = axios.create({
  timeout: 1000000,
});
request.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    return config;
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  }
);
request.interceptors.response.use(
  function (response) {
    // 对响应数据做点什么
    // 把response的data返回给客户端, 不需要可以删除下面1句代码
    return response;
  },
  function (error) {
    // 对响应错误做点什么
    return Promise.reject(error);
  }
);
export default request;
