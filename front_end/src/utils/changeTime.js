let byTime = [365 * 24 * 60 * 60 * 1000, 24 * 60 * 60 * 1000, 60 * 60 * 1000, 60 * 1000, 1000];
let unit = ['年', '天', '小时', '分钟', '秒钟'];
function changeTime(atime) {
  let dif = new Date().getTime() - new Date(atime).getTime();
  if (dif < 0) {
    return false;
  }
  let time = [];
  for (let i = 0; i < byTime.length; i++) {
    if (dif < byTime[i]) {
      continue;
    }
    let temp = Math.floor(dif / byTime[i]);
    dif = dif % byTime[i];
    if (temp > 0) {
      time.push(temp + unit[i]);
    }
    if (time.length >= 1) {
      break;
    }
  }
  return time.join('') + '前';
}
export default changeTime;
