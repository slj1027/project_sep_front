/* 分页器 */
import React from 'react';
import { Pagination } from 'antd';
import './scss/index.min.css';
function Page({ current, total, pageSize, onChange }) {
  return (
    <div className='page'>
      {total && (
        <Pagination current={current} total={total} pageSize={pageSize} onChange={onChange} />
      )}
    </div>
  );
}

export default Page;
