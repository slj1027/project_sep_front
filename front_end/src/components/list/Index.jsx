/* 列表页 */
import React from 'react';
import { EyeOutlined, ShareAltOutlined, HeartOutlined } from '@ant-design/icons'; //icon图标
import changeTime from '../../utils/changeTime'; //时间格式
import './scss/index.min.css';
function Index({ arr, setShow, setDisplay, toDetail, statusCode }) {
  return (
    <div>
      {arr && arr.length > 0 ? (
        arr.map((item, index) => {
          return (
            <div
              key={index}
              className='article_item'
              onClick={() => {
                toDetail(item);
              }}
            >
              <h4>
                <b>{item.title}</b>
                <span>{changeTime(item.publishAt)}</span>
              </h4>
              <div className='article_cover'>
                {item.cover && (
                  <div
                    className='cover_img'
                    style={{
                      backgroundImage: 'url(' + item.cover + ')',
                      backgroundSize: 'cover',
                      backgroundPosition: '50%',
                    }}
                  ></div>
                )}

                <div>
                  <div>
                    <p className='cover_p'> {item.summary}</p>
                    <p className='cover_q'>
                      <b>
                        <HeartOutlined />
                        {item.likes}
                      </b>
                      <b>
                        <EyeOutlined />
                        {item.views}
                      </b>
                      <b
                        onClick={(e) => {
                          e.stopPropagation();
                          setDisplay();
                          setShow(item);
                        }}
                      >
                        <ShareAltOutlined />
                        share
                      </b>
                    </p>
                  </div>
                </div>
              </div>
            </div>
          );
        })
      ) : (
        <p style={{ textAlign: 'center', lineHeight: '80px' }}>暂无数据，敬请期待...</p>
      )}
    </div>
  );
}

export default Index;
