/* 顶部导航 */
import { GET_FOOT } from '../../actions';
import { Layout, BackTop, Modal } from 'antd';
import React, { useState, useEffect } from 'react';
import { WifiOutlined, GithubOutlined } from '@ant-design/icons';
import { Outlet } from 'react-router-dom';
import Search from '../search/Index';
import Head from './components/Head';
import Login from '../../components/login/Index';
import { useDispatch, useSelector } from 'react-redux';
import style from './css/style.module.css';
import './index.min.css';
const { Header, Content, Footer } = Layout;

function TopHeader() {
  const dispatch = useDispatch();
  // 派发底部数据
  useEffect(() => {
    dispatch(GET_FOOT());
  }, [dispatch]);
  useEffect(() => {
    localStorage.getItem('like')
      ? localStorage.setItem('like', localStorage.getItem('like'))
      : localStorage.setItem('like', JSON.stringify([]));
  }, []);
  // 从仓库拿数据
  const { data: footData } = useSelector((state) => state.article.footData);
  // *控制搜索页面显示与取消
  const [show, setShow] = useState(false);
  //监听底部
  // const [di, setDi] = useState(false);
  const onClose = () => setShow(false);
  const onOpen = () => setShow(true);
  //*登录弹框
  const [isModalOpen, setIsModalOpen] = useState(false);
  //展示登录弹框
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <Layout id='index' className={style.theme}>
      <Header id='top-header'>
        <Head showModal={showModal} onOpen={onOpen} />
      </Header>
      <Content id='middle-content'>
        <Outlet />
      </Content>
      <Footer id='foot' /* style={{ opacity: di ? 1 : 0 }} */>
        {footData && (
          <ul className='foot_ul'>
            <li>
              <a href='/rss'>
                <WifiOutlined className='icon' style={{ fontSize: '20px', color: '#fff' }} />
              </a>
              <a href='https://github.com/fantasticit/wipi'>
                <GithubOutlined
                  className='icon'
                  style={{ fontSize: '20px', marginLeft: '10px', color: '#fff' }}
                />
              </a>
            </li>
            <li>{footData.systemFooterInfo}</li>
          </ul>
        )}
      </Footer>
      {show && <Search onClose={onClose}></Search>}
      <BackTop></BackTop>
      <Modal title='登录' open={isModalOpen} onOk={handleOk} onCancel={handleCancel} footer={null}>
        <Login close={handleCancel} />
      </Modal>
    </Layout>
  );
}

export default TopHeader;
