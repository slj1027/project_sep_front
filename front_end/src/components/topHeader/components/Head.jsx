import React, { useState, useEffect } from 'react';
import { SearchOutlined, AlignLeftOutlined, TranslationOutlined } from '@ant-design/icons';
import { Row, Col, Menu, Dropdown } from 'antd';
import { NavLink } from 'react-router-dom';
// *i18n
import i18next from '../../../i18n';
import { useTranslation } from 'react-i18next';
import { getNewsPage, view } from '../../../api';
const theme = () => {
  if (window.localStorage.getItem('theme')) {
    return window.localStorage.getItem('theme');
  } else {
    if (new Date().getHours() > 6 && new Date().getHours() < 19) {
      return 'light';
    } else {
      return 'dark';
    }
  }
};
localStorage.setItem('theme', theme());
localStorage.setItem('user', JSON.stringify({ name: '', password: '', email: '' }));
function Head({ showModal, onOpen }) {
  const [img, setImg] = useState('');
  // *切换语言（国际化）
  const { t } = useTranslation();
  useEffect(() => {
    i18next.changeLanguage(navigator.language);
  }, []);
  /* const [lang, setLanguage] = useState(() => (navigator.language === 'en' ? 'zh-CN' : 'en')); */
  const changeLanguage = (language) => {
    i18next.changeLanguage(language);
  };
  let getView = async () => {
    let { data } = await view();
    setImg(data.data.systemLogo);
  };
  useEffect(() => {
    getView();
  }, []);
  //*切换主题
  useEffect(() => {
    theme();
  }, []);
  const [ico, setIco] = useState(() => (theme() === 'light' ? true : false));
  useEffect(() => {
    document.documentElement.className = localStorage.getItem('theme');
  }, []);
  const changeTheme = () => {
    setIco(!ico);
    if (theme() === 'light') {
      localStorage.setItem('theme', 'dark');
    } else {
      localStorage.setItem('theme', 'light');
    }
    document.documentElement.className = localStorage.getItem('theme');
  };
  //*下拉菜单（响应式布局）
  const menu1 = (
    <Menu
      items={[
        {
          key: '1',
          label: <span onClick={() => changeLanguage('en')}>{t('中文')}</span>,
        },
        {
          key: '2',
          label: <span onClick={() => changeLanguage('zh-CN')}>{t('英文')}</span>,
        },
      ]}
    />
  );
  const menu = (
    <Menu
      items={[
        {
          key: '1',
          label: (
            <a style={{ color: '#000' }} target='_self' rel='noopener noreferrer' href='/article'>
              {t('文章')}
            </a>
          ),
        },
        {
          key: '2',
          label: (
            <a style={{ color: '#000' }} target='_self' rel='noopener noreferrer' href='/archives'>
              {t('归档')}
            </a>
          ),
        },
        {
          key: '3',
          label: (
            <a style={{ color: '#000' }} target='_self' rel='noopener noreferrer' href='/knowledge'>
              {t('知识小册')}
            </a>
          ),
        },
        {
          key: '4',
          label: (
            <div>
              <Dropdown overlay={menu1}>
                <TranslationOutlined style={{ fontSize: '1.2rem' }} />
              </Dropdown>
            </div>
          ),
        },
        {
          key: '5',
          label: <div onClick={() => changeTheme()}>{ico ? '☀️' : '🌙'}</div>,
        },
        {
          key: '6',
          label: (
            <div
              onClick={() => {
                onOpen();
              }}
            >
              <SearchOutlined />
            </div>
          ),
        },
      ]}
    />
  );

  const [page, setPage] = useState([]);
  let getPage = async () => {
    let data = await getNewsPage({ status: 'publish' });
    console.log(data.data.data, '11111111111111111111111111');
    setPage(data.data.data);
  };
  useEffect(() => {
    getPage();
  }, []);
  return (
    <Row id='wrap' justify='flex-start'>
      <Col
        xs={{ span: 1, offset: 0 }}
        sm={{ span: 1, offset: 0 }}
        md={{ span: 1, offset: 0 }}
        lg={{ span: 1, offset: 0 }}
      >
        <img src={img} alt='' width={'50px'} />
      </Col>
      <Col
        xs={{ span: 0, offset: 0 }}
        sm={{ span: 0, offset: 0 }}
        md={{ span: 16, offset: 1 }}
        lg={{ span: 16, offset: 1 }}
      >
        <Row justify='flex-start'>
          <Col>
            <NavLink
              to={'/article'}
              style={({ isActive }) => (isActive ? { color: '#ff0055' } : {})}
            >
              {t('文章')}
            </NavLink>
          </Col>
          <Col offset={1}>
            <NavLink
              to={'/archives'}
              style={({ isActive }) => (isActive ? { color: '#ff0055' } : {})}
            >
              {t('归档')}
            </NavLink>
          </Col>
          <Col offset={1}>
            <NavLink
              to={'/knowledge'}
              style={({ isActive }) => (isActive ? { color: '#ff0055' } : {})}
            >
              {t('知识小册')}
            </NavLink>
          </Col>
          {page &&
            page[0] &&
            page[0].length > 0 &&
            page[0].map((item) => {
              return (
                <Col offset={1} key={item.id}>
                  <NavLink
                    to={'/page/' + item.path}
                    style={({ isActive }) => (isActive ? { color: '#ff0055' } : {})}
                  >
                    {item.name}
                  </NavLink>
                </Col>
              );
            })}
        </Row>
      </Col>
      <Col
        xs={{ span: 0, offset: 0 }}
        sm={{ span: 0, offset: 0 }}
        md={{ span: 6, offset: 0 }}
        lg={{ span: 6, offset: 0 }}
      >
        <Row justify='flex-start' align='middle'>
          <Col offset={0} span={3}>
            <div>
              <Dropdown overlay={menu1}>
                <TranslationOutlined style={{ fontSize: '1.2rem' }} />
              </Dropdown>
            </div>
          </Col>
          <Col offset={0} span={4}>
            <div onClick={() => changeTheme()}>{ico ? '☀️' : '🌙'}</div>
          </Col>
          <Col offset={1} span={3}>
            <SearchOutlined onClick={onOpen} style={{ fontSize: '1.2rem' }} />
          </Col>
          <Col offset={1} span={10}>
            <button onClick={showModal} style={{ fontSize: '1.2rem' }}>
              {t('登录')}
            </button>
          </Col>
        </Row>
      </Col>
      <Col
        xs={{ span: 1, offset: 22 }}
        sm={{ span: 1, offset: 22 }}
        md={{ span: 0, offset: 22 }}
        lg={{ span: 0, offset: 0 }}
      >
        <Dropdown overlay={menu} placement='bottomLeft'>
          <AlignLeftOutlined style={{ color: '##ff0055' }} />
        </Dropdown>
      </Col>
    </Row>
  );
}

export default Head;
