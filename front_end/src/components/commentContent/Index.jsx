/* eslint-disable react-hooks/rules-of-hooks */
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Comment, Avatar } from 'antd';
import Loading from '../loading/Index';
import changeTime from '../../utils/changeTime';
import CommentInp from '../comment/Index';
import Page from '../pagination/Index';
import { GET_COMMENT } from '../../actions';
import { useParams } from 'react-router';
import './scss/index.min.css';
function CommentContent({ type }) {
  const dispatch = useDispatch();
  const params = useParams();
  const [reply, setReply] = useState({});
  const { statusCode, data: comment } = useSelector((state) => state.article.comment);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState(6);
  useEffect(() => {
    dispatch(
      GET_COMMENT(params.id, {
        page,
        pageSize,
      })
    );
  }, [dispatch, page, pageSize, params.id]);
  const onChange = (page, pageSize) => {
    setPage(page);
    setPageSize(pageSize);
    dispatch(
      GET_COMMENT(params.id, {
        page,
        pageSize,
      })
    );
  };
  const render = (arr) =>
    arr.map((item, index) => {
      return (
        <Comment
          key={index}
          actions={[
            <span
              key='comment-nested-reply-to'
              onClick={() => {
                dispatch({
                  type: 'showCommentInp',
                  payload: item.id,
                });
                setReply(item);
              }}
            >
              Reply to
            </span>,
          ]}
          author={
            <span>
              {item.name} 回复 {item.replyUserName}
            </span>
          }
          avatar={
            <Avatar
              style={{
                backgroundColor: '#' + Math.random().toString(16).slice(-6),
                verticalAlign: 'middle',
                fontSize: '20px',
              }}
              size='large'
            >
              {item.name.slice(0, 1)}
            </Avatar>
          }
          content={
            <div>
              <p>{item.content}</p>
              <p>
                {item.userAgent}·{changeTime(item.updateAt)}
              </p>
            </div>
          }
        >
          {item.show && <CommentInp reply={reply} type={type} />}
          {item.children && render(item.children)}
        </Comment>
      );
    });
  return (
    <div id='comment'>
      <Loading code={statusCode}>
        <div>{comment && comment[0] && render(comment[0])}</div>
        {comment && comment[0] && comment[0].length > 0 ? (
          <Page onChange={onChange} total={comment[1]} pageSize={pageSize} current={page} />
        ) : (
          <div style={{ textAlign: 'center' }}>暂无评论，请前去评论</div>
        )}
      </Loading>
    </div>
  );
}

export default CommentContent;
