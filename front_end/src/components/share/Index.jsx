import React, { useRef } from 'react';
import { Button, Modal } from 'antd';
import useCanvas from '../../hook/useCanvas';
import QRCode from 'qrcode';
import './scss/index.min.css';
import { useState } from 'react';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { getPoster } from '../../actions';
function Index({ item, isModalOpen, handleOk, handleCancel }) {
  const canvasDom = useRef(null);
  const poster = useRef(null);
  const [url, setUrl] = useState();
  const canvasUrl = useCanvas(item, canvasDom, url);
  useEffect(() => {
    QRCode.toDataURL(item.title, async (err, url) => {
      setUrl(url);
    });
  }, [item.title]);
  const dispatch = useDispatch();
  return (
    <Modal
      title={item.title}
      open={isModalOpen}
      onCancel={handleCancel}
      footer={[
        <Button key='back' onClick={() => handleCancel()}>
          取消
        </Button>,
        <Button
          key='download'
          type='primary'
          onClick={(e) => {
            dispatch(
              getPoster({
                width: 475,
                height: 400,
                html: poster.current.outerHTML,
                name: item.title,
              })
            );
          }}
        >
          <a
            href={canvasUrl}
            download={new Date() * 1 + '.png'}
            onClick={(e) => {
              /* e.target.download = new Date() * 1 + '.png'; */
            }}
          >
            下载
          </a>
        </Button>,
      ]}
    >
      <div className='poster' ref={poster}>
        {item.cover && (
          <div
            style={{
              backgroundImage: 'url(' + item.cover + ')',
              backgroundSize: 'cover',
              backgroundPosition: '50%',
              height: '240px',
            }}
          ></div>
        )}
        <h2>{item.title}</h2>
        <div className='qrcode'>
          <img src={url} alt='' />
          <div>
            <p>识别二维码，查看文章</p>
            <p></p>
          </div>
        </div>
      </div>
      <canvas ref={canvasDom} style={{ display: 'none' }}></canvas>
    </Modal>
  );
}

export default Index;
