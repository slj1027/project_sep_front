import { Spin } from 'antd';
import React from 'react';

function Loading({ className, code, children }) {
  return (
    <div className={className}>
      {code >= 200 && code < 400 ? (
        children && children
      ) : code >= 400 ? (
        <div style={{ textAlign: 'center' }}>页面/数据出错了</div>
      ) : (
        <div style={{ textAlign: 'center' }}>
          <Spin />
        </div>
      )}
    </div>
  );
}

export default Loading;
