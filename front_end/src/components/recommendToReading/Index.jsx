/* 侧边推荐阅读 */
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getRecommendToReading, setView } from '../../actions';
import changeTime from '../../utils/changeTime';
import Loading from '../loading/Index';
import './scss/index.min.css';
function RecommendToReading() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getRecommendToReading('publish'));
  }, [dispatch]);
  const { statusCode, data: recommendToReading } = useSelector(
    (state) => state.article.recommendToReading
  );
  return (
    <div className='recommendToReading'>
      <h4>推荐阅读</h4>
      <Loading code={statusCode}>
        <ul>
          {recommendToReading &&
            recommendToReading.map((item) => {
              return (
                <li
                  key={item.id}
                  onClick={() => {
                    dispatch(setView('https://creation.shbwyz.com/article/' + item.id));
                  }}
                >
                  <a href={'/detail/' + item.id}>
                    <span>{item.title}</span>
                    <time dateTime={item.publishAt}>·大约{changeTime(item.publishAt)}</time>
                  </a>
                </li>
              );
            })}
        </ul>
      </Loading>
    </div>
  );
}

export default RecommendToReading;
