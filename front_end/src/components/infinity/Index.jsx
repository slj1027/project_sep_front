import React from 'react';
import InfiniteScroll from 'react-infinite-scroll-component'; //滚动加载
import { Spin } from 'antd'; //引入antd组件

function InfinityToScroll(props) {
  const { length, children, fetchMoreData, hasMore } = props;
  return (
    <InfiniteScroll /* 滚动加载 */
      dataLength={length}
      hasMore={hasMore}
      next={fetchMoreData}
      loader={
        <h4 style={{ textAlign: 'center' }}>
          <Spin />
        </h4>
      }
      endMessage={
        <p style={{ textAlign: 'center' }}>
          <b>数据已经全部加载完毕，更多精彩敬请期待...</b>
        </p>
      }
    >
      {children}
    </InfiniteScroll>
  );
}

export default InfinityToScroll;
