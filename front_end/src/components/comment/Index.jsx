/* 评论区域 */
import React, { useState } from 'react';
import Login from '../login/Index';
import emoji from 'emojis-list';
import { ADD_COMMENT } from '../../actions';
import { Row, Col, Input, Button, message, Modal } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import './scss/index.min.css';
const { TextArea } = Input;
function Comment({ reply, type }) {
  const dispatch = useDispatch();
  const [show, setShow] = useState(false);
  const [value, setValue] = useState('');
  const user = useSelector((state) => state.article.user);
  // const user = JSON.parse(localStorage.getItem('user'));
  const { data: articleDetail } = useSelector((state) => state.article.articleDetail);
  const params = useParams();
  //*登录弹窗
  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    setIsModalOpen(false);
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  return (
    <div className='comment-inp'>
      <TextArea
        value={value}
        rows={10}
        onInput={(e) => {
          if (user) {
            setValue(e.target.value);
          }
        }}
        onClick={() => {
          if (!user.name) {
            showModal();
          }
        }}
        placeholder='请输入评论，评论支持markdown形式'
      />
      <Row justify='space-between' align='middle'>
        <Col style={{ position: 'relative' }}>
          <div onMouseOver={() => setShow(true)}>😃</div>
          {show && (
            <div className='emoji' onMouseLeave={() => setShow(false)}>
              {emoji.map((item, index) => (
                <span key={index} onClick={() => setValue(value + emoji[index])}>
                  {item}
                </span>
              ))}
            </div>
          )}
        </Col>
        <Col>
          <Button
            disabled={!value || !articleDetail.isCommentable ? true : false}
            onClick={() => {
              let data = {
                email: user.email,
                name: user.name,
                content: value,
                url: type === 'page' ? `/page/${params.id}` : `/article/${params.id}`,
                hostId: params.id,
              };
              // *二级回复
              if (reply) {
                data.parentCommentId = reply.id;
                data.replyUserEmail = reply.email;
                data.replyUserName = reply.name;
              }
              if (articleDetail.isCommentable) {
                dispatch(ADD_COMMENT(data));
                message.success('发布成功，请等待审核');
              } else {
                message.error('该消息不允许评论');
              }
            }}
          >
            发表
          </Button>
        </Col>
        <Modal
          title='登录'
          open={isModalOpen}
          onOk={handleOk}
          onCancel={handleCancel}
          footer={null}
        >
          <Login close={handleCancel} />
        </Modal>
      </Row>
    </div>
  );
}

export default Comment;
