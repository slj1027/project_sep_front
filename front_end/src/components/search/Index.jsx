/* 搜索页 */
import React, { useEffect, useState } from 'react';
import { Input } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { getSearchArticle } from '../../actions';
import Loading from '../loading/Index';
import { useNavigate } from 'react-router';
import './scss/index.min.css';
const { Search } = Input;
function SearchInp({ onClose }) {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [show, setShow] = useState(false);
  useEffect(() => {
    window.onkeydown = (e) => {
      if (e.keyCode === 27) {
        onClose();
      }
    };
  });
  const [value, setValue] = useState('');
  const { statusCode, data: searchArticle } = useSelector((state) => state.article.searchArticle);
  const search = () => {
    setShow(true);
    dispatch(getSearchArticle(value));
  };
  return (
    <div className='search'>
      <div className='main'>
        <h3>文章搜索</h3>
        <div onClick={onClose} className='search-esc'>
          esc
        </div>
        <div className='search-inp'>
          <Search
            placeholder='searchArticle'
            onSearch={() => search()}
            onInput={(e) => setValue(e.target.value)}
          />
        </div>
        {show && (
          <Loading className='search-content' code={statusCode}>
            {searchArticle &&
              searchArticle.length > 0 &&
              searchArticle.map((item, index) => {
                return (
                  <div
                    key={index}
                    onClick={() => {
                      navigate('/detail/' + item.id);
                      setValue('');
                      onClose();
                    }}
                  >
                    {item.title}
                  </div>
                );
              })}
          </Loading>
        )}
      </div>
    </div>
  );
}

export default SearchInp;
