/* 登录组件 */
import React from 'react';
import { Button, Form, Input } from 'antd';
import { useDispatch } from 'react-redux';
import { Get_USER } from '../../actions';
function Login({ close }) {
  const dispatch = useDispatch();
  const onFinish = (values) => {
    dispatch(Get_USER(values));
    // values.email = '1111111@qq.com';

    close();
  };
  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };
  return (
    <div className='login'>
      <Form
        className='login_form'
        name='basic'
        labelCol={{
          span: 6,
        }}
        wrapperCol={{
          span: 16,
        }}
        initialValues={{
          remember: true,
        }}
        onFinish={onFinish}
        onFinishFailed={onFinishFailed}
        autoComplete='off'
      >
        <Form.Item
          label='name'
          name='name'
          rules={[
            {
              required: true,
              message: 'Please input your username!',
            },
          ]}
        >
          <Input />
        </Form.Item>
        <Form.Item
          label='Password'
          name='password'
          rules={[
            {
              required: true,
              message: 'Please input your password!',
            },
          ]}
        >
          <Input.Password />
        </Form.Item>
        <Form.Item
          wrapperCol={{
            offset: 8,
            span: 16,
          }}
        >
          <>
            <Button type='dangerous' onClick={close}>
              取消
            </Button>
            <Button type='primary' htmlType='submit'>
              Submit
            </Button>
          </>
        </Form.Item>
      </Form>
    </div>
  );
}

export default Login;
