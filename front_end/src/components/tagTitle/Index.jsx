import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getTag } from '../../actions';
import Loading from '../loading/Index';
import './scss/index.min.css';
function TagTitle() {
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getTag());
  }, [dispatch]);
  const { statusCode, data: tag } = useSelector((state) => state.article.tag);
  return (
    <div className='tagTitle'>
      <h4>文章标题</h4>
      <Loading code={statusCode}>
        <div className='tagTitle-content'>
          {tag &&
            tag.length > 0 &&
            tag.map((item, i) => {
              return (
                <div key={i} className='tagTitle-item'>
                  <a href={'/tags/' + item.value}>
                    <span>{item.label}</span>
                    <span>[{item.articleCount}]</span>
                  </a>
                </div>
              );
            })}
        </div>
      </Loading>
    </div>
  );
}

export default TagTitle;
