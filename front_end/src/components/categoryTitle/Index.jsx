import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getCategoryTitle } from '../../actions';
import Loading from '../loading/Index';
import './scss/index.min.css';
function CategoryTitle() {
  const dispatch = useDispatch();
  const { statusCode, data: categoryTitle } = useSelector((state) => state.article.categoryTitle);
  useEffect(() => {
    dispatch(getCategoryTitle('publish'));
  }, [dispatch]);
  return (
    <div className='category'>
      <h4>文章分类</h4>
      <Loading code={statusCode}>
        {categoryTitle && categoryTitle.length > 0 ? (
          categoryTitle.map((item, index) => {
            return (
              <div key={item.id}>
                <p>
                  <a href={'/category/' + item.value}>
                    <span>{item.label}</span>
                    <span>共计 {item.articleCount} 篇文章</span>
                  </a>
                </p>
              </div>
            );
          })
        ) : (
          <div className='empty'>empty</div>
        )}
      </Loading>
    </div>
  );
}

export default CategoryTitle;
