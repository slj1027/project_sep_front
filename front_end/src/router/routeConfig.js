import { lazy } from 'react';

const routes = [
  {
    path: '/',
    redirect: '/article', // 重定向:重新指向其它path,会改变网址
  },
  {
    path: '/',
    component: lazy(() => import('../components/topHeader/TopHeader.jsx')),
    children: [
      {
        path: '/article',
        component: lazy(() => import('../views/article/Index')),
        name: 'article',
      },
      {
        path: '/archives',
        component: lazy(() => import('../views/archives/Index')),
        name: 'archives',
      },
      {
        path: '/knowledge',
        component: lazy(() => import('../views/knowledge/Index')),
        name: 'knowledge',
      },
      {
        path: '/page/:id',
        component: lazy(() => import('../views/page/Index')),
        name: 'page',
      },
      {
        path: '/tags/:id',
        component: lazy(() => import('../views/tags/Index')),
        name: 'tags',
      },
      {
        path: '/category/:id',
        component: lazy(() => import('../views/category/index')),
        name: 'category',
      },
      {
        path: '/detail/:id',
        component: lazy(() => import('../views/detail/Index')),
        name: 'detail',
      },
      {
        path: '/particulars/:id',
        component: lazy(() => import('../views/particulars/Particulars')),
        name: 'particulars',
      },
      {
        path: '*',
        component: lazy(() => import('../views/noFound/Index')),
      },
    ],
  },
];
export default routes;
