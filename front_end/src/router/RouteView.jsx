import React, { Suspense } from 'react';
import { BrowserRouter, Navigate, Route, Routes } from 'react-router-dom';
// import { view } from '../api';
import routes from './routeConfig';
function RouteView() {
  const render = (arr) => {
    return arr.map((item, index) => {
      return (
        <Route
          key={index}
          path={item.path}
          element={item.redirect ? <Navigate to={item.redirect} /> : <item.component />}
        >
          {item.children && render(item.children)}
        </Route>
      );
    });
  };
  return (
    <Suspense
      fallback={<div style={{ textAlign: 'center', lineHeight: '100px' }}>拼命加载中...</div>}
    >
      <BrowserRouter>
        <Routes>{render(routes)}</Routes>
      </BrowserRouter>
    </Suspense>
  );
}

export default RouteView;
