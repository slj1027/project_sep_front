const initialState = {
  user: localStorage.getItem('user') ? JSON.parse(localStorage.getItem('user')) : {}, //用户
  data: [],
  know: {}, //知识书
  knowledgeItem: {},
  knowSlice: [],
  archives: [], //档案
  tag: [], //标题
  tagItem: {},
  tagArticleItem: {},
  recommendToReading: [],
  categoryTitle: [],
  categoryItem: [],
  artCateList: [],
  searchArticle: [], //搜索
  articleDetail: {},
  SwiperData: [],
  SwiperDataButton: [],
  comment: [], //评论
  footData: {}, //底部数据
  pageItem: {},
};

const article = (state = initialState, { type, payload }) => {
  let newState = JSON.parse(JSON.stringify(state));
  switch (type) {
    case 'GET_PAGE_ITEM':
      return {
        ...state,
        pageItem: payload,
      };
    case 'GET_DATA':
      newState.data = payload;
      return newState;
    //知识小册
    case 'GET_KNOW':
      return {
        ...state,
        know: payload,
      };
    case 'GET_knowledgeItem':
      console.log(state.know, 222);

      let newKnow = [...state.know.data];
      let newList = newKnow[0].filter((item, index) => {
        // id相同时取反
        return item.id !== payload;
      });
      let newKnowItem = newKnow[0].filter((item, index) => {
        // id相同时取反
        return item.id === payload;
      });
      return {
        ...state,
        knowledgeItem: { ...newKnowItem[0] },
        knowSlice: newList,
      };
    // 档案
    case 'GET_ARCH':
      newState.archives = payload;
      return newState;
    //用户
    case 'Get_USER':
      console.log(payload);
      localStorage.setItem('user', JSON.stringify(payload.data));
      return { ...state, user: payload.data };
    case 'GET_TAG':
      return { ...state, tag: payload };
    case 'GET_TAG_ITEM':
      return { ...state, tagItem: payload };
    case 'GET_TAG_Article_ITEM':
      return { ...state, tagArticleItem: payload };
    case 'GET_RecommendToReading':
      return { ...state, recommendToReading: payload };
    case 'GET_CategoryTitle':
      return { ...state, categoryTitle: payload };
    case 'GET_CATEGORYITEM':
      return { ...state, categoryItem: payload };
    case 'GET_ARTCATE':
      return { ...state, artCateList: payload };
    case 'GET_FOOT':
      return { ...state, footData: payload };
    case 'GET_Search_Article':
      state.searchArticle.data = [];
      return { ...state, searchArticle: payload };
    case 'SET_Article_Likes':
      localStorage.getItem('like')
        ? localStorage.setItem('like', localStorage.getItem('like'))
        : localStorage.setItem('like', JSON.stringify([]));
      let arr = JSON.parse(localStorage.getItem('like'));
      if (arr.indexOf(payload) === -1) {
        arr.push(payload);
      } else {
        arr.splice(arr.indexOf(payload), 1);
      }
      localStorage.setItem('like', JSON.stringify(arr));
      return { ...state };
    case 'GET_Article_Detail':
      return { ...state, articleDetail: payload };
    case 'Get_Swiper':
      return { ...state, SwiperData: payload };
    case 'Get_SwiperButton':
      return { ...state, SwiperDataButton: payload };
    case 'Get_COMMENT':
      return { ...state, comment: payload };
    case 'showCommentInp':
      let newComment = { ...state.comment };
      const showCommentInp = (arr) => {
        arr.forEach((item, index) => {
          if (item.id === payload) {
            item.show = !item.show;
          } else {
            if (item.children) {
              showCommentInp(item.children);
            }
          }
        });
      };
      showCommentInp(newComment.data[0]);
      return { ...state, comment: { ...newComment } };
    default:
      return { ...state };
  }
};
export default article;
