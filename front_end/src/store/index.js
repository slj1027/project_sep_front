import { legacy_createStore, applyMiddleware, combineReducers } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import article from './module/article';
let all = combineReducers({
  article,
});

export default legacy_createStore(all, applyMiddleware(logger, thunk));
