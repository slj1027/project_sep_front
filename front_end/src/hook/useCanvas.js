import { useState, useEffect } from 'react';
const loadImg = (url) => {
  return new Promise((resolve, reject) => {
    let img = new Image();
    img.onload = () => {
      resolve(img);
    };
    img.onerror = (error) => reject(error);
    img.src = url;
    img.crossOrigin = 'anonymous';
  });
};
const drawUrl = async (ctx, data, canvasDom, url) => {
  ctx.fillStyle = '#fff';
  ctx.fillRect(0, 0, 450, 679);
  data.cover && ctx.drawImage(await loadImg(data.cover), 0, 0, 400, 300);
  ctx.font = '30px Verdana';
  ctx.fillStyle = '#000';
  ctx.fillText(data.title, 10, 350);
  ctx.beginPath();
  ctx.moveTo(10, 370);
  ctx.lineTo(390, 370);
  ctx.stroke();
  ctx.font = '14px Verdana';
  ctx.fillStyle = '#000';
  ctx.fillText('识别二维码，查看文章', 220, 450);
  url && ctx.drawImage(await loadImg(url), 5, 370, 200, 200);
  const imgUrl = canvasDom.current.toDataURL();
  return imgUrl;
};
const createCanvas = async (data, canvasDom, url) => {
  canvasDom.current.width = 400;
  canvasDom.current.height = 679;
  const ctx = canvasDom.current.getContext('2d');
  let imgUrl = await drawUrl(ctx, data, canvasDom, url);
  return imgUrl;
};
function useCanvas(data, canvasDom, img) {
  const [url, setUrl] = useState('');
  useEffect(() => {
    (async () => {
      let url = await createCanvas(data, canvasDom, img);
      setUrl(url);
    })();
  }, [data, canvasDom, img]);
  return url;
}

export default useCanvas;
