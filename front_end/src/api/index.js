import request from '../utils/request';
export const pay = (data) =>
  request({
    method: 'POST',
    url: '/pay/pay',
    data: data,
  });
export const psw = (id, password) =>
  request({
    method: 'POST',
    url: `/api/article/${id}/checkPassword`,
    data: { password },
  });
export const view = () =>
  request({
    method: 'POST',
    url: '/api/setting/get',
  });
export const createPoster = (data) =>
  request({
    method: 'POST',
    url: '/api/poster',
    data: data,
  });
export const setViewAdd = (url) =>
  request({
    method: 'POST',
    url: '/api/view',
    data: { url },
  });
export const article = ({ page, pageSize, status = 'publish' }) =>
  request({
    method: 'GET',
    url: '/api/article',
    params: { page, pageSize, status },
  });
//*知识接口
export const knowledgeBooks = () =>
  request({
    method: 'GET',
    url: '/api/Knowledge',
  });
//*档案接口
export const archives = () =>
  request({
    method: 'GET',
    url: '/api/article/archives',
  });
//*tagTitle
export const tag = () =>
  request({
    method: 'GET',
    url: '/api/tag',
  });
//*标签详情
export const tagItem = (id) =>
  request({
    method: 'GET',
    url: `/api/tag/${id}`,
  });
export const tagArticleItem = (id, page = 1, pageSize = 12, status = 'publish') =>
  request({
    method: 'GET',
    url: `/api/article/tag/${id}`,
    params: { page, pageSize, status },
  });
//*推荐阅读
export const recommendToReading = () =>
  request({
    method: 'GET',
    url: `/api/article/recommend`,
  });
export const categoryTitle = (articleStatus = 'publish') =>
  request({
    method: 'GET',
    url: `/api/category`,
    params: { articleStatus },
  });
//详情
export const categoryItem = (id) =>
  request({
    method: 'GET',
    url: `/api/category/${id}`,
  });
export const artCate = (id) =>
  request({
    method: 'GET',
    url: `/api/article/category/${id}`,
  });
//*搜索
export const searchArticle = (keyword) =>
  request({
    method: 'GET',
    url: `/api/search/article`,
    params: { keyword },
  });
//*喜欢
export const articleLikes = (id, type) =>
  request({
    method: 'POST',
    url: `/api/article/${id}/likes`,
    data: { type },
  });
//*文章详情页数据
export const detailArticle = (id) =>
  request({
    method: 'POST',
    url: `/api/article/${id}/views`,
  });

export const Article = () =>
  request({
    method: 'GET',
    url: `/api/article`,
  });

export const ArticleButton = () =>
  request({
    method: 'GET',
    url: `/api/category`,
    params: {
      articleStatus: 'publish',
    },
  });
//登录获取用户
export const userInfo = (params) =>
  request({
    method: 'POST',
    url: `/api/auth/login`,
    data: params,
  });
//发布评论
export const commentAdd = (params) =>
  request({
    method: 'POST',
    url: `/api/comment`,
    data: params,
  });
export const commentAll = (id, data) =>
  request({
    method: 'GET',
    url: `/api/comment/host/${id}`,
    params: { data },
  });
export const knowledgeItem = (id) =>
  request({
    method: 'get',
    url: `/api/knowledge//${id}`,
  });
//底部
export const foot = () =>
  request({
    method: 'POST',
    url: '/api/setting/get',
  });
export const getNewsPage = (params) =>
  request({
    method: 'GET',
    url: '/api/page',
    params,
  });
export const getNewsPageItem = (id) =>
  request({
    method: 'GET',
    url: `/api/page/${id}`,
  });
