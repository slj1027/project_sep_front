import {
  article,
  tag,
  tagItem,
  tagArticleItem,
  recommendToReading,
  categoryTitle,
  categoryItem,
  artCate,
  searchArticle,
  detailArticle,
  knowledgeBooks,
  Article,
  archives,
  ArticleButton,
  userInfo,
  commentAll,
  knowledgeItem,
  articleLikes,
  setViewAdd,
  commentAdd,
  createPoster,
  foot,
  getNewsPageItem,
} from '../api';
export const getPoster = (data) => {
  return async (dispatch) => {
    let res = await createPoster(data);
    dispatch({
      type: 'GET_Poster',
      payload: { data: res },
    });
  };
};
export const setView = (url, str = '', id = 1) => {
  return async (dispatch) => {
    let data = await setViewAdd(url);
    if (str === 'know') {
      dispatch({
        type: 'GET_knowledgeItem',
        payload: id,
      });
    } else {
      dispatch({
        type: 'GET_Article_Detail',
        payload: data.data,
      });
    }
  };
};
export const getKnowledgeItem = (id) => {
  return async (dispatch) => {
    let data = await knowledgeItem(id);
    dispatch({
      type: 'GET_knowledgeItem',
      payload: { data: data.data, id: id },
    });
  };
};
export const getData = ({ page, pageSize, status }) => {
  return async (dispatch) => {
    let data = await article({ page, pageSize, status });
    dispatch({
      type: 'GET_DATA',
      payload: data.data,
    });
  };
};

// 知识书 方法
export const get_list = () => {
  return async (dispatch) => {
    // 获取数据
    let data = await knowledgeBooks();
    dispatch({
      type: 'GET_KNOW',
      payload: data.data,
    });
  };
};
export const get_knowledgeItem = (id) => {
  return async (dispatch) => {
    dispatch({
      type: 'GET_knowledgeItem',
      payload: id,
    });
  };
};

export const getTag = () => {
  return async (dispatch) => {
    let data = await tag();
    dispatch({
      type: 'GET_TAG',
      payload: data.data,
    });
  };
};
export const getTagItem = (id) => {
  return async (dispatch) => {
    let data = await tagItem(id);
    dispatch({
      type: 'GET_TAG_ITEM',
      payload: data.data,
    });
  };
};
export const getTagArticleItem = (id, page, pageSize, status) => {
  return async (dispatch) => {
    let data = await tagArticleItem(id, page, pageSize, status);
    dispatch({
      type: 'GET_TAG_Article_ITEM',
      payload: data.data,
    });
  };
};
export const getRecommendToReading = (id, page = 1, pageSize = 12, status = 'publish') => {
  return async (dispatch) => {
    let data = await recommendToReading(id, page, pageSize, status);
    dispatch({
      type: 'GET_RecommendToReading',
      payload: data.data,
    });
  };
};
export const getCategoryTitle = (articleStatus = 'publish') => {
  return async (dispatch) => {
    let data = await categoryTitle(articleStatus);
    dispatch({
      type: 'GET_CategoryTitle',
      payload: data.data,
    });
  };
};
//详情
export const getCategoryItem = (id) => {
  return async (dispatch) => {
    let data = await categoryItem(id);
    dispatch({
      type: 'GET_CATEGORYITEM',
      payload: data.data,
    });
  };
};
export const getArtCate = (id) => {
  return async (dispatch) => {
    let data = await artCate(id);
    dispatch({
      type: 'GET_ARTCATE',
      payload: data.data,
    });
  };
};
// 档案 方法
export const get_archives = () => {
  return async (dispatch) => {
    // 获取数据
    let data = await archives();
    dispatch({
      type: 'GET_ARCH',
      payload: data.data,
    });
  };
};
export const getSearchArticle = (keyword = '') => {
  return async (dispatch) => {
    let data = await searchArticle(keyword);
    dispatch({
      type: 'GET_Search_Article',
      payload: data.data,
    });
  };
};
export const setArticleLikes = (id) => {
  return async (dispatch) => {
    dispatch({
      type: 'SET_Article_Likes',
      payload: id,
    });
  };
};
export const getDetailArticle = (id) => {
  return async (dispatch) => {
    let data = await detailArticle(id);
    dispatch({
      type: 'GET_Article_Detail',
      payload: data.data,
    });
  };
};
export const getDetailArticleLike = (id, type) => {
  return async (dispatch) => {
    let data = await articleLikes(id, type);
    dispatch({
      type: 'GET_Article_Detail',
      payload: data.data,
    });
  };
};
export const Get_Swiper = () => {
  return async (dispatch) => {
    let data = await Article();
    dispatch({
      type: 'GET_Swiper',
      payload: data.data,
    });
  };
};

export const Get_SwiperButton = () => {
  return async (dispatch) => {
    let data = await ArticleButton();
    dispatch({
      type: 'Get_SwiperButton',
      payload: data.data,
    });
  };
};
export const Get_USER = (params) => {
  return async (dispatch) => {
    let data = await userInfo(params);
    dispatch({
      type: 'Get_USER',
      payload: data.data,
    });
  };
};
export const ADD_COMMENT = (params) => {
  return async (dispatch) => {
    let data = await commentAdd(params);
    dispatch({
      type: 'ADD_COMMENT',
      payload: data.data,
    });
  };
};
export const GET_COMMENT = (id, params) => {
  return async (dispatch) => {
    let data = await commentAll(id, params);
    dispatch({
      type: 'Get_COMMENT',
      payload: data.data,
    });
  };
};

//底部
export const GET_FOOT = () => {
  return async (dispatch) => {
    let data = await foot();
    dispatch({
      type: 'GET_FOOT',
      payload: data.data,
    });
  };
};
//
export const GET_PAGE_ITEM = (id) => {
  return async (dispatch) => {
    let data = await getNewsPageItem(id);
    dispatch({
      type: 'GET_PAGE_ITEM',
      payload: data.data,
    });
  };
};
